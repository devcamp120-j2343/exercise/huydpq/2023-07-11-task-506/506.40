const getAllVoucher = (req, res, next) => {
    console.log("Get All Voucher");
    next()
};
const getVoucher = (req, res, next) => {
    console.log("Get a Voucher");
    next();
};
const postVoucher = (req, res, next) => {
    console.log("Post a Voucher");
    next();
}
const putVoucher = (req, res, next) => {
    console.log("Put a Voucher");
    next();
}
const deleteVoucher = (req, res, next) => {
    console.log("Delete a Voucher");
    next();
}

module.exports = {
    getAllVoucher,
    getVoucher,
    postVoucher,
    putVoucher,
    deleteVoucher
}