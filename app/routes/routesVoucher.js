const express = require("express")

const  {
    getAllVoucher,
    getVoucher,
    postVoucher,
    putVoucher,
    deleteVoucher
} = require ("../middleware/middlewareVoucher");

const router = express.Router();

router.get("/" ,getAllVoucher ,(req, res) => {
    res.json({
        message: "Get all Voucher"
    })
});

router.get("/get/:vocherId" ,getVoucher ,(req, res) => {
    let vocherId = req.params.vocherId
    res.json({
        message: `Voucher ID: ${vocherId}`
    })
});
router.post("/post" ,postVoucher ,(req, res) => {
    res.json({
        message: "POST a Voucher"
    })
});
router.put("/put" ,putVoucher ,(req, res) => {
    res.json({
        message: "PUT a Voucher"
    })
});
router.delete("/delete" ,deleteVoucher ,(req, res) => {
    res.json({
        message: "DELETE a Voucher"
    })
});

module.exports = {router};