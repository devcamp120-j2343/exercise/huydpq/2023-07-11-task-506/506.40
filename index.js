const express = require("express");
const { router } = require("./app/routes/routesVoucher");

const app = express();

const port = 8000;

app.use(router);

app.listen(port, () => {
    console.log(`Đang chay cổng: ${port}`);
})